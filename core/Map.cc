/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond – initial implementation
 *
 ******************************************************************************/
#include "Map.hh"

int map_get_max_elements(int n_elements)
{
  if (n_elements <= 0) {
    return 0;
  }
  int max_elements = 2;
  while (max_elements < n_elements) {
    max_elements *= 2;
  }
  return max_elements;
}
