/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/
#ifndef PERAST_HH
#define PERAST_HH

#include "../datatypes.h"

class PerAST {
public:
  boolean extendable;
  int* field_order;
  int nof_ext_adds;
  ext_add_info* ext_add_indexes;
  int* enum_values;
  int* enum_codes;
  PerAST();
  ~PerAST();
};

#endif /* PERAST_HH */
