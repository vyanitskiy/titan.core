/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond – initial implementation
 *
 ******************************************************************************/
module ReverseOrderInit {

type component CT {
  timer tmr := 0.01;
}

type record Rec1 {
  Rec2 f1,
  integer f2
}

type record Rec2 {
  integer f1,
  charstring f2
}

template Rec1 t_main := { f_temp(), 10 };

function f_temp() runs on CT return template Rec2 {
  var Rec2 v := valueof(t1);
  var template Rec2 vt := t2;
  {
    vt := t3;
  }
  f_other(t4);
  tmr.start;
  alt {
    [] as(t7);
    [else] {
      while (v.f1 > 0) {
        v.f1 := v.f1 - valueof(t12.f1);
      }
    }
  }
  tmr.stop;
  return t14;
}

function f_other(in template Rec2 p) {
  log(t5);
  action(t6);
}

altstep as(in template Rec2 p) runs on CT {
  var Rec2 v := valueof(t8);
  var template Rec2 vt := t9;
  [] tmr.timeout {
    if (v.f1 > 0) {
      for (var integer i := valueof(t10.f1); i < valueof(t11.f1); i := i + valueof(t12.f1)) {
        string2ttcn(valueof(t13.f2), v.f1);
      }
    }
  }
}

template Rec2 t1 := { 1, "a" };
template Rec2 t2 := { 2, "a" };
template Rec2 t3 := { 3, "a" };
template Rec2 t4 := { 4, "a" };
template Rec2 t5 := { 5, "a" };
template Rec2 t6 := { 6, "a" };
template Rec2 t7 := { 7, "a" };
template Rec2 t8 := { 8, "a" };
template Rec2 t9 := { 9, "a" };
template Rec2 t10 := { 10, "a" };
template Rec2 t11 := { 11, "a" };
template Rec2 t12 := { 12, "a" };
template Rec2 t13 := { 13, "1" };
template Rec2 t14 := { 14, "a" };

testcase tc_reverseOrderInit() runs on CT {
  var template Rec1 t_exp := { t14, 10 };
  if (log2str(t_main) != log2str(t_exp)) {
    setverdict(fail, "Expected: ", t_exp, ", got: ", t_main);
  }
  else {
    setverdict(pass);
  }
}

control {
  execute(tc_reverseOrderInit());
}

}
