/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/

#include "any_type_param.hh"

namespace any__type__param {

INTEGER fx__value(const ANY_TYPE& p__data)
{
  if (strcmp(p__data.name, "integer") == 0) {
    return (*(INTEGER*)(p__data.data));
  }
  else if (strcmp(p__data.name, "charstring") == 0) {
    return ((CHARSTRING*)(p__data.data))->lengthof();
  }
  else if (strcmp(p__data.name, "@any_type_param.Rec") == 0) {
    return ((Rec*)(p__data.data))->num();
  }
  else if (strcmp(p__data.name, "@any_type_param.IntList") == 0) {
    return ((IntList*)(p__data.data))->size_of();
  }
  TTCN_error("Unrecognized parameter type in external function `fx_value'");
}

INTEGER fx__template(const ANY_TEMPLATE_TYPE& p__data)
{
  if (strcmp(p__data.name, "integer") == 0) {
    return ((INTEGER_template*)(p__data.data))->valueof();
  }
  else if (strcmp(p__data.name, "charstring") == 0) {
    return ((CHARSTRING_template*)(p__data.data))->valueof().lengthof();
  }
  else if (strcmp(p__data.name, "@any_type_param.Rec") == 0) {
    return ((Rec_template*)(p__data.data))->valueof().num();
  }
  else if (strcmp(p__data.name, "@any_type_param.IntList") == 0) {
    return ((IntList_template*)(p__data.data))->valueof().size_of();
  }
  TTCN_error("Unrecognized parameter type in external function `fx_template'");
}

void fx__ref(ANY_TYPE& p__ref, ANY_TEMPLATE_TYPE& p__ret)
{
  if (strcmp(p__ref.name, "integer") == 0 &&
      strcmp(p__ret.name, "integer") == 0) {
    INTEGER* i = (INTEGER*)(p__ref.data);
    (*(INTEGER_template*)(p__ret.data)) = *i;
    ++(*i);
  }
  else if (strcmp(p__ref.name, "charstring") == 0 &&
           strcmp(p__ret.name, "charstring") == 0) {
    CHARSTRING* c = (CHARSTRING*)(p__ref.data);
    (*(CHARSTRING_template*)(p__ret.data)) = *c;
    (*c) += "x";
  }
  else if (strcmp(p__ref.name, "@any_type_param.Rec") == 0 &&
           strcmp(p__ret.name, "@any_type_param.Rec") == 0) {
    Rec* r = (Rec*)(p__ref.data);
    (*(Rec_template*)(p__ret.data)) = *r;
    ++r->num();
    r->str() += "x";
  }
  else if (strcmp(p__ref.name, "@any_type_param.IntList") == 0 &&
           strcmp(p__ret.name, "@any_type_param.IntList") == 0) {
    IntList* l = (IntList*)(p__ref.data);
    (*(IntList_template*)(p__ret.data)) = *l;
    for (int i = 0; i < l->size_of(); ++i) {
      ++((*l)[i]);
    }
  }
  else {
    TTCN_error("Unrecognized parameter type in external function `fx_ref'");
  }
}

}
